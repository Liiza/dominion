import copy
import json
import random
from argparse import ArgumentParser
from collections import defaultdict
from itertools import groupby
from timeit import default_timer as timer



def args():
    class ResourceRange(object):
        def __init__(self, value):
            self.value = value
        def __call__(self):
            bounds = list(map(int, self.value.split('..')))
            if len(bounds) == 1:
                bounds = (bounds[0], bounds[0]+1)
            else:
                bounds = (bounds[0], bounds[1]+1, 1 if len(bounds) < 3 else bounds[2])
            return range(*bounds)
    parser = ArgumentParser()
    parser.add_argument('--debug', default=0, type=int)
    parser.add_argument('--games', default=30, type=int)
    parser.add_argument('--top', default=30, type=int)
    parser.add_argument('--golds', default='5..16', type=ResourceRange)
    parser.add_argument('--silvers', default='3..16', type=ResourceRange)
    parser.add_argument('--militias', default='0..8', type=ResourceRange)
    parser.add_argument('--moneylenders', default='0..2', type=ResourceRange)
    parser.add_argument('--villages', default='0..6', type=ResourceRange)
    return parser.parse_args()


DEBUG = args().debug
class LogIndent:
    log_indentation = 0
    previous_had_newline = True
    def __enter__(self):
        LogIndent.log_indentation += 1
    def __exit__(self, exc_type, exc_val, exc_tb):
        LogIndent.log_indentation -= 1

    @classmethod
    def message(cls, level, *args, end="\n"):
        if DEBUG >= level:
            if not cls.previous_had_newline or cls.log_indentation == 0:
                indentation = ''
            else:
                indentation = Color.DIM + ' ' * (cls.log_indentation - 1) + ' - '
            print(indentation, end=Color.END)
            print(*args, end=end)
            cls.previous_had_newline = (end == "\n")
        return LogIndent()

def info(*args, end="\n"):
    return LogIndent.message(1, *args, end=end)
def debug(*args):
    return LogIndent.message(2, *args, end=end)


class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    DIM = "\033[2m"
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


CARDS_IN_HAND = 5


class RunOutOfStockException(Exception):
    def __init__(self):
        pass


class Card:
    def __init__(self, name, price, value=None, action=False, treasure=False, victory=False):
        self.price = price
        self.value = value
        self.name = name
        self.action = action
        self.treasure = treasure
        self.victory = victory

    def card_type(self):
        if self.treasure:
            return "treasure"
        elif self.action:
            return "action"

    def __str__(self):
        return self.name

    def __add__(self, other):
        return str(self) + other

    def __radd__(self, other):
        return other + str(self)

    def __lt__(self, other):
        return str(self) < str(other)


class Village(Card):
    def __init__(self):
        super().__init__(Color.CYAN + "Village" + Color.END, 3, 0, action=True)

    def play(self, hand, deck=None, player=None, victims=[]):

        player.actions += 2
        with info(f"{player.short_name} plays {self}"):
            if deck.cards:
                hand.append(deck.draw())
                info(f"drew a {hand[-1]}")


class MoneyLender(Card):
    def __init__(self):
        super().__init__(Color.CYAN + "MoneyLender" + Color.END, 4, 0, action=True)

    def play(self, hand, deck=None, player=None, victims=[]):
        to_trash = -1

        for index, item in enumerate(hand):
            if type(item) == Copper:
                to_trash = index
                break

        if to_trash > -1:
            # We want to permanentally delete the copper card.
            assert type(hand[to_trash]) == Copper
            del hand[to_trash]

            info(f"{player.short_name} plays {self} (+$3)")
            player.extra_money += 3
        else:
            # This is okay, sometimes you might want to discard an
            # action card even if it has no effect to make space in your hand.
            info("Playing moneylender even when no copper in hand")


class Militia(Card):
    PROFIT = 2

    def __init__(self):
        super().__init__(Color.CYAN + "Militia" + Color.END, 4, 0, action=True)

    def play(self, hand, deck=None, player=None, victims=[]):

        with info(f"{player.short_name} plays {self} (+${Militia.PROFIT})"):
            player.extra_money += Militia.PROFIT
            for victim in victims:
                cards = victim.discard_cards(3)


class Gold(Card):
    def __init__(self):
        super().__init__(Color.YELLOW + "Gold" + Color.END, 6, value=3, treasure=True)


class Silver(Card):
    def __init__(self):
        super().__init__(Color.YELLOW + "Silver" + Color.END, 3, value=2, treasure=True)


class Copper(Card):
    def __init__(self):
        super().__init__(Color.YELLOW + "Copper" + Color.END, 0, value=1, treasure=True)


class Province(Card):
    def __init__(self):
        super().__init__(Color.GREEN + "Province" + Color.END, 8, value=6, victory=True)


class Duchy(Card):
    def __init__(self):
        super().__init__(Color.GREEN + "Duchy" + Color.END, 5, value=3, victory=True)


class Estate(Card):
    def __init__(self):
        super().__init__(Color.GREEN + "Estate" + Color.END, 3, value=1, victory=True)


class Deck:
    def __init__(self, cards, shuffle=True):
        self.cards = cards
        if shuffle:
            random.shuffle(cards)

    def draw(self):
        return self.cards.pop()

    def add(self, card):
        return self.cards.append(card)

    def shuffle(self):
        random.shuffle(cards)

    def len(self):
        return len(self.cards)

    def get_victory_cards(self):
        return [card for card in self.cards if card.victory]


class Board:
    def __init__(self):
        self.cards = {
            Gold: 30,
            Silver: 40,
            Province: 10,
            Duchy: 10,
            MoneyLender: 10,
            Village: 10,
            Militia: 10
        }
        self.game_over = False

    def can_buy(self, money, action=True, victory=True, treasure=True):
        cards = []
        for card_type in self.cards.keys():
            card = card_type()
            if (card.action and action) or (card.treasure and treasure) or (card.victory and victory):
                if card.price <= money and self.cards[card_type] > 0:
                    cards.append(card_type)
        return cards

    def buy(self, type):
        if type not in Card.__subclasses__():
            raise Exception("Type not defined " + type)

        if type not in self.cards:
            raise Exception("Card " + type + " is not availabe this time")

        if self.cards[type] == 0:
            raise RunOutOfStockException()

        self.cards[type] = self.cards[type] - 1

        if self.cards[Province] == 0:
            info("Game over, all provinces bought.")
            self.game_over = True
        return type()


def cards_str(cards):
    return " ".join(map(str, cards))


class Player():
    def __init__(self, col, name, preferred_cards=[]):
        self.name = col + name + Color.END
        self.short_name = col + name[0] + Color.END
        self.hand = []
        self.deck = Deck([
            Copper(),
            Copper(),
            Copper(),
            Copper(),
            Copper(),
            Copper(),
            Copper(),
            Estate(),
            Estate(),
            Estate()
        ])
        assert self.deck.len() >= CARDS_IN_HAND
        self.discard = []
        # Some action cards make you gain extra money you can use by that turn
        self.extra_money = 0
        self.actions = 1
        # What are the action cards this player prefers.
        self.preferred_cards = preferred_cards

    def grouped_cards(self, cards=None):
        if cards is None:
            cards = (self.discard + self.hand + self.deck.cards)
        grouped_counts_and_cards = [(len(list(group[1])), group[0]) for group in groupby(sorted(cards), key=lambda card: card.name)]
        return ", ".join([f"{str('' if count == 1 else f'{count} ')}{card}{'' if count == 1 else 's'}" for (count, card) in grouped_counts_and_cards])

    def discard_cards(self, cards_left_in_hand):
        assert len(self.hand) > 0

        usable = list(self.usable_action_cards())

        def card_comparator(card):
            return [
                card.victory,
                card.action and (card not in usable or (usable.index(card) - 100)),
                (card.treasure and -card.value) or -4
            ]

        how_many_discard = len(self.hand) - cards_left_in_hand
        sorted_hand = sorted(self.hand, key=card_comparator)
        info(f'{self.short_name} discards ', end='')
        discarded_cards = []
        for i in range(how_many_discard):
            to_discard = sorted_hand.pop()
            discarded_cards.append(to_discard)
            self.hand.remove(to_discard)
            self.discard.append(to_discard)
        info(self.grouped_cards(discarded_cards), end='.')
        info(" Remaining hand:", self.grouped_cards(self.hand))

    def get_victory_points(self):
        victory_cards = self.deck.get_victory_cards()
        victory_cards.extend([card for card in self.discard if card.victory])
        victory_cards.extend([card for card in self.hand if card.victory])
        return sum([card.value for card in victory_cards])

    def get_cards(self, card_type, cards=None):
        if cards is None:
            cards = (self.discard + self.hand + self.deck.cards)
        return [card for card in cards if type(card) == card_type]

    def discard_to_deck(self):
        if self.deck.len() > 0:
            raise Exception("Deck is not empty. Can't start taking cards from discard")
        self.deck = Deck(self.discard, len(self.discard))
        self.discard = []

    def draw_hand(self):
        if DEBUG == 2:
            print(f"{self.short_name} discard pile: {' '.join(map(str, self.discard))} ({len(self.discard)})")
            print(f"{self.short_name} deck: {' '.join(map(str, self.deck.cards))} ({len(self.deck.cards)})")

        assert len(self.hand) == 0

        for i in range(CARDS_IN_HAND):
            if self.deck.len() == 0:
                if DEBUG == 2:
                    print(f"{self.short_name} shuffles their deck")
                self.discard_to_deck()
            if self.deck.len() == 0:
                # Somehow we ended up with less than five cards in play
                raise Exception("less than five cards in play " + self.grouped_cards() + " " + self.name)
                break
            self.hand.append(self.deck.draw())

        info(self.short_name, "draws", self.grouped_cards(self.hand), " money:", self.hand_value())

    def play(self, board, other_players=[]):
        self.action(other_players)
        self.buy(board)
        self.clean_up()

    def usable_action_cards(self, actions_left=1):
        hand_cards_to_consider = set(self.hand)
        hand_cards_considered = set()

        while actions_left:
            hand_cards_to_consider.update(card for card in self.hand if card not in hand_cards_considered)

            moneylenders = self.get_cards(MoneyLender, hand_cards_to_consider)
            militias = self.get_cards(Militia, hand_cards_to_consider)
            villages = self.get_cards(Village, hand_cards_to_consider)

            if villages:
                card = villages[0]
                actions_left += 2
            elif militias:
                card = militias[0]
            elif moneylenders and self.should_use_moneylender():
                card = moneylenders[0]
            else:
                break

            hand_cards_to_consider.remove(card)
            hand_cards_considered.add(card)
            yield card
            actions_left -= 1

    def action(self, other_players):
        # Action phase
        action_cards = self.usable_action_cards(self.actions)

        for action_card in action_cards:
            if self.actions:
                self.actions -= 1
            else:
                break

            action_card.play(self.hand, self.deck, self, other_players)
            self.move_card_discard(action_card)

    def move_card_discard(self, card):
        prior_remove = len(self.hand)
        self.hand.remove(card)
        assert prior_remove - 1 == len(self.hand)
        self.discard.append(card)

    def should_use_moneylender(self):
        coppers = [card for card in self.hand if type(card) == Copper]
        if not coppers:
            return

        # Money if uses moneylender
        money = self.hand_value() + 2
        for card_type, max_count in self.preferred_cards.items():
            if money >= card_type().price and len(self.get_cards(card_type)) < max_count:
                return True
        return False

    def treasure_cards(self):
        return [card for card in self.hand if card.treasure]

    def hand_value(self):
        hand_value = sum([card.value for card in self.treasure_cards()]) + self.extra_money
        return hand_value

    def buy(self, board, card_type=None):
        info(f"{self.short_name} plays {self.grouped_cards(self.treasure_cards())} (+${sum(card.value for card in self.treasure_cards())})")

        # Buying phase
        hand_value = self.hand_value()

        if card_type:
            info("Buying the adviced card_type " + card_type)
            self.discard.append(board.buy(card_type))
            return

        for card_type, max_count in self.preferred_cards.items():
            try:
                if hand_value >= card_type().price and len(self.get_cards(card_type)) < max_count:
                    self.discard.append(board.buy(card_type))
                    info(f"{self.short_name} bought {self.discard[-1]}")
                    break
            except RunOutOfStockException:
                pass

    def clean_up(self):
        self.extra_money = 0
        self.actions = 1
        self.discard.extend(self.hand)
        self.hand = []
        self.draw_hand()


def max_vps_can_make(player, board, turns_left):
    if turns_left == 0:
        return player.get_victory_points()
    # Action phase
    player.action([])
    cards_available_to_buy = board.can_buy(player.hand_value())
    if not cards_available_to_buy:
        player.clean_up()
        return max_vps_can_make(player, board, turns_left - 1)
    max_vps = 0
    for card in cards_available_to_buy:
        player_cp = copy.deepcopy(player)
        board_cp = copy.deepcopy(board)
        player_cp.buy(board_cp, card)
        # Ending the turn discarding the hand etc.
        # Drawing new hand
        player_cp.clean_up()
        vps = max_vps_can_make(player_cp, board_cp, turns_left - 1)
        if max_vps < vps:
            max_vps = vps
    return max_vps


def which_card_should_buy(player, board):
    cards_available_to_buy = board.can_buy(player.hand_value())
    max_vps = 0
    best_card = None
    for card in cards_available_to_buy:
        player_cp = copy.deepcopy(player)
        board_cp = copy.deepcopy(board)
        player_cp.buy(board_cp, card)
        # Ending the turn discarding the hand etc.
        # Drawing new hand
        player_cp.clean_up()
        vps = max_vps_can_make(player_cp, board_cp, 3)
        if vps > max_vps:
            max_vps = vps
            best_card = card
    return best_card


def who_wins(number_of_actions_cards, preferred_cards):
    board = Board()
    player = Player(Color.RED, "Liisa", number_of_actions_cards, preferred_cards)
    # How many turns does it take to end the game
    turns = 0
    while not board.game_over:
        info(player.name)
        turns += 1
        player.play(board)

    board = Board()
    player_r = Player(Color.RED, "Raimo", 0)
    turns_r = 0
    while not board.game_over:
        info(player_r.name)
        turns_r += 1
        player_r.play(board)

    info("It took player " + player.name + " " + str(turns) + " turns to end the game")
    info("It took player " + player_r.name + " " + str(turns_r) + " turns to end the game")
    if turns == turns_r:
        return "Tie"
    return "Liisa" if turns < turns_r else "Raimo"


def play_ai():
    board = Board()
    player = Player(Color.BLUE, "Liisa-AI")
    # How many turns does it take to end the game
    turns = 0
    while not board.game_over:
        global DEBUG
        info("~~~~")
        info(player.name)
        turns += 1
        player.action([])
        back_up = DEBUG
        DEBUG = 0
        card_type = which_card_should_buy(player, board)
        print("Recommended card " + str(card_type))
        DEBUG = back_up
        player.buy(board, card_type=card_type)
        player.clean_up()

    print("It took player " + player.name + " " + str(turns) + " turns to end the game")

    return "Liisa-AI"


def simulate_game_between(player, player_r):
    board = Board()

    player, player_r = random.sample([player, player_r], 2)
    # There are curses that impact other players hand like
    # let's you trash other players cards, so let's draw both hands first.
    player.draw_hand()
    player_r.draw_hand()
    turn = 1
    while not board.game_over:
        info(Color.BOLD + "Turn " + str(turn) + " - " + player.name)
        player.play(board, [player_r])

        if board.game_over:
            break
        info(Color.BOLD + "Turn " + str(turn) + " - " + player_r.name)
        player_r.play(board, [player])
        turn += 1

    player_victory_points = player.get_victory_points()
    player_r_victory_points = player_r.get_victory_points()
    info("Player " + player.name + " has " + str(player_victory_points) + " victory points")
    info("Player " + player_r.name + " has " + str(player_r_victory_points) + " victory points")

    if player_victory_points < player_r_victory_points:
        player, player_r = player_r, player
    return [player, player_r, player_victory_points == player_r_victory_points]


def simulate_game_between_strategies(one, two):
    player = Player(Color.RED, f"Liisa-g{one[Gold]}s{one[Silver]}m{one[Militia]}", preferred_cards=one)
    player_r = Player(Color.BLUE, f"Raimo-g{two[Gold]}s{two[Silver]}m{two[Militia]}ml{two[MoneyLender]}v{two[Village]}", preferred_cards=two)
    return simulate_game_between(player, player_r)


def valid_strategy(strategy):
    # Unable to buy at least gold.
    if sum(sorted([2] * strategy[Silver] + [2 if strategy[Militia] else 0] * 7 + [2])[-5:]) < 6:
        return False
    # Unable to buy a province.
    if sum(sorted([3] * strategy[Gold] + [2] * strategy[Silver] + [Militia.PROFIT if strategy[Militia] else 0] * 7 + [2])[-5:]) < 8:
        return False
    return True


def range_progress(r):
    start = last_output = timer()
    for i, elem in enumerate(r):
        if timer() - last_output > 3:
            percentage_done = i / len(r)
            time_elapsed = timer() - start
            time_left = time_elapsed / percentage_done - time_elapsed
            print(f"{round(100 * percentage_done)} % ready. Estimated time left: {round(time_left)} seconds")
            last_output = timer()
        yield elem


class Stats:
    def __init__(self):
        self.stats = defaultdict(lambda: (0, 0, 0))
        self.example_deck = defaultdict(lambda: None)

    def __str__(self):
        stat_table = [
            (event_name, wins, ties, losses, round(100 * (wins+ties/2)/(wins+ties+losses)))
            for event_name, (wins, ties, losses) in self.stats.items()
        ]

        top_stats = sorted(stat_table, key=lambda stat: -stat[4])[:args().top]

        return "\n".join(
            f'{event_name}: {wins} wins, '
            f'{ties} ties, '
            f'{losses} losses '
            f'in {wins+ties+losses} games. '
            f'Success: {success_ratio} %. '
            f'Last Deck {self.example_deck[event_name]}'
            for event_name, wins, ties, losses, success_ratio in top_stats
        )

    def add(self, event_name, delta, last_deck):
        self.stats[event_name] = tuple(map(sum, zip(self.stats[event_name], delta)))
        self.example_deck[event_name] = last_deck

    def add_win(self, event_name, last_deck): self.add(event_name, (1, 0, 0), last_deck)
    def add_loss(self, event_name, last_deck): self.add(event_name, (0, 0, 1), last_deck)
    def add_tie(self, event_name, last_deck): self.add(event_name, (0, 1, 0), last_deck)


def main():
    # wins_by_amount_of_money_lenders = {}
    # for j in range(10):
    # 	amount_of_money_lenders = j + 1
    # 	wins = {"Liisa": 0, "Raimo": 0, "Tie": 0}
    # 	for i in range(100):
    # 		name_of_winner = who_wins(1, [MoneyLender])
    # 		wins[name_of_winner] += j
    # 	print(wins)
    # 	if wins["Liisa"] == wins["Raimo"]:
    # 		wins_by_amount_of_money_lenders[amount_of_money_lenders] = "Tie"
    # 	else:
    # 		winner = "Liisa" if wins["Liisa"] > wins["Raimo"] else "Raimo"
    # 		wins_by_amount_of_money_lenders[amount_of_money_lenders] = winner
    # print(wins_by_amount_of_money_lenders)

    liisa = {Province: 10, Gold: 20, Militia: 20, Silver: 20}
    assert valid_strategy(liisa)

    stats = Stats()

    start = timer()
    for g in range_progress(args().golds()):
        for s in args().silvers():
            for m in args().militias():
                for ml in args().moneylenders():
                    for v in args().villages():
                        for _ in range(args().games):
                            contestant = {Province: 10, Gold: g, Militia: m, Silver: s, Village: v, MoneyLender: ml}
                            if not valid_strategy(contestant): continue

                            winner, loser, tie = simulate_game_between_strategies(liisa, contestant)
                            if tie:
                                stats.add_tie(winner.name, winner.grouped_cards())
                                stats.add_tie(loser.name, loser.grouped_cards())
                            else:
                                stats.add_win(winner.name, winner.grouped_cards())
                                stats.add_loss(loser.name, loser.grouped_cards())

    print(stats)
    print(f"Simulations took {timer()-start} seconds")

    # play_ai()


if __name__ == '__main__':
    main()
